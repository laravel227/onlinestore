<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function update(){
        DB::update("
            UPDATE users SET name='alen',
            email='alen@gmail.com' WHERE id=8
        ");
        return redirect("/users");
    }
    public function delete(){
        DB::delete("DELETE FROM users WHERE id=10");
        return redirect("/users");
    }
    public function select(){
        $users = DB::select("SELECT * FROM users ORDER BY id DESC");
        foreach($users as $user){
            echo $user->id . " | ";
            echo $user->name . " | ";
            echo $user->gender . " | ";
            echo $user->email . "<hr>";
        }
    }
    public function insert(){
        DB::insert("
            INSERT INTO users(name, gender, email)
            VALUES('veasna', 'm', 'veasna@yahoo.com')
        ");
        return 'Record has been inserted successully';
    }
}
