<h1>Please fill all the fields</h1>
<form action="/items" method="POST">
    @csrf
    <label for="item_name">Item name:</label>
    <input type="text" name="item_name" id="item_name">
    <br><br>

    <label for="price">Item price:</label>
    <input type="text" name="price" id="price">
    <br><br>

    <input type="submit" value="Add New Item">
</form>